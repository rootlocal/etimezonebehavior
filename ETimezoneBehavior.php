<?php

/**
 * Support for custom Users Time Zones
 * @see CActiveRecordBehavior
 * @package application.components.behaviors
 * @link https://github.com/rootlocal/ETimezoneBehavior
 */
class ETimezoneBehavior extends CActiveRecordBehavior
{

	/**
	 * Attributes this Model
	 * This Time Zone from UTC Time (of Greenwich)
	 * @var array
	 */
	public $attributes = array();

	/**
	 * Timezone
	 * @var string
	 */
	public $timezone = null;

	/**
	 * Default Timezone
	 * @var string
	 */
	public $default_timezone = 'Europe/Moscow';

	/**
	 * data/time format types
	 * @var boolean
	 * values are: TIMESTAMP if not then ISO8601,  Y-m-d H:i:s or etc.
	 * Defaults: TIMESTAMP
	 */
	public $UnixTime = true;

	/**
	 * Out Data Format
	 * @var string
	 * @example 'Y-m-d H:i:s'
	 */
	public $OutDataFormat = 'Y-m-d H:i:s';

	/**
	 * user ID
	 * @var integer
	 */
	public $userId = false;

	/**
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::afterFind()
	 */
	public function afterFind($event)
	{

		$owner = $this->getOwner();

		if(!Yii::app()->user->isGuest){

			if($this->timezone === null){
				if($this->userId === false){
					$this->userId = Yii::app()->user->id;
				}

				if(Yii::app()->user->getState('timezone') !== null){
					$this->timezone = Yii::app()->user->timezone;
				}
				else{
					$this->timezone = $this->default_timezone;
				}

			}
		}
		else{
			$this->timezone = $this->default_timezone;
		}

		if($this->timezone == '' || $this->timezone === null){
			$this->timezone = $this->default_timezone;
		}

		foreach($this->attributes as $key => $attr){
			if(isset($owner->$attr)){

				if($this->UnixTime === true){
					$UTCDate = date('Y-m-d H:i:s', $owner->$attr);
				}
				else{
					$UTCDate = $owner->$attr;
				}

				$date = new DateTime($UTCDate);

				$date->setTimezone(new DateTimeZone($this->timezone));

				if($this->UnixTime === true){
					$newDate = strtotime($date->format('Y-m-d H:i:s'));
				}
				else{
					$newDate = $date->format($this->OutDataFormat);
				}

				$owner->setAttribute($attr, $newDate);
			}
		}
	}
}