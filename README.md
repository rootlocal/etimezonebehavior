ETimezoneBehavior
=================

_Yii Support for custom Users Time Zones_

## example:

### protected/config/main.php
```php
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web application',
	'timeZone' => 'UTC',
// ...
);
```

### postgresql.conf
~~~
timezone = 'UTC'
~~~

### table user

```sql
CREATE TABLE public.tbl_user (
	id SERIAL PRIMARY KEY NOT NULL,
	username character varying(128) NOT NULL,
	password character varying(128) NOT NULL,
	email character varying(128) NOT NULL,
	create_time integer DEFAULT round(date_part('epoch'::text, now())),
	update_time integer DEFAULT round(date_part('epoch'::text, now())),
	timezone character varying(128)
);

CREATE UNIQUE INDEX tbl_user_username_key ON tbl_user USING btree (username);
CREATE UNIQUE INDEX tbl_user_email_key ON tbl_user USING btree (email);
```

### table post
```sql
CREATE TABLE public.tbl_post (
	id SERIAL PRIMARY KEY NOT NULL,
	title character varying(128) NOT NULL,
	text text,
	user_id integer NOT NULL,
	create_time integer DEFAULT round(date_part('epoch'::text, now())),
	update_time integer DEFAULT round(date_part('epoch'::text, now())),

	FOREIGN KEY (user_id) REFERENCES tbl_user (id) ON UPDATE CASCADE ON DELETE SET NULL);

CREATE INDEX tbl_post_user_id_key ON tbl_post USING BTREE (user_id);
```

### function t_update_time()

```sql
CREATE OR REPLACE FUNCTION t_update_time() RETURNS TRIGGER AS $$
DECLARE
-- this timestamp unix
curtime integer := round(date_part('epoch'::text, now()));
BEGIN

-- if insert
	IF TG_OP = 'INSERT' THEN
		if(NEW.create_time = NULL) then
			NEW.create_time = curtime;
		end if;
		if(NEW.update_time = NULL) then
			NEW.update_time = curtime;
		end if;
	return NEW;

-- if update
	ELSIF TG_OP = 'UPDATE' THEN
		NEW.update_time = curtime;
		if(NEW.create_time = NULL) then
			NEW.create_time = curtime;
		end if;
	return NEW;

	END IF;
-- end
END;
$$ LANGUAGE plpgsql;
```

### Trigger tbl_user_trigger_update_time
```sql
CREATE TRIGGER tbl_user_trigger_update_time
	BEFORE INSERT OR UPDATE ON tbl_user
		FOR EACH ROW EXECUTE PROCEDURE t_update_time();
```

### Trigger tbl_post_trigger_update_time
```sql
CREATE TRIGGER tbl_post_trigger_update_time
	BEFORE INSERT OR UPDATE ON tbl_post
		FOR EACH ROW EXECUTE PROCEDURE t_update_time();
```

### Model User
```php
class User extends CActiveRecord
{
// ...

	public function behaviors() {
		return array(
			'timezone' => array(
				'class' => 'app.components.behaviors.ETimezoneBehavior.ETimezoneBehavior',
				'attributes' => array('create_time', 'update_time'),
			),
		);
	}
	
	protected function afterSave(){

		Yii::app()->user->timezone = $this->timezone;
		return parent::afterSave();
	}

// ...
}
```

### Model Post
```php
class Post extends CActiveRecord
{
// ...

    public function behaviors() {
		return array(
			'timezone' => array(
				'class' => 'app.components.behaviors.ETimezoneBehavior.ETimezoneBehavior',
				'attributes' => array('create_time', 'update_time'),
			),
		);
	}

// ...
}
```
